import java.util.Objects;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction first = new Lfraction(1, 2);
      System.out.println(first.hashCode());

      Lfraction second = new Lfraction(4, 8);
      System.out.println("first.fractionPart() : " + first.fractionPart());
      System.out.println("second.fractionPart() : " + second.fractionPart());

      System.out.println("first.toDouble() : " + first.toDouble());
      System.out.println("second.toDouble() : " + second.toDouble());

      System.out.println("first equals second : " + first.equals(second));

      System.out.println(Math.PI * 7);
      long num = (long) (Math.PI * 7 + 0.5);
      System.out.println(num);

      System.out.println(-10.);
      long num2 = (long) (-10. * 2 - 0.5);
      System.out.println(num2);

      Lfraction third = new Lfraction(-3, 9);
      Lfraction fourth = new Lfraction(1, 3);
      System.out.println("third : " + third);
      System.out.println("fourth : " + fourth);

      Lfraction fifth = new Lfraction(0, 5);
      System.out.println(fifth);
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b <= 0) {
         throw new RuntimeException("denominator must be more than 0");}

      numerator = a;
      denominator = b;

      simplify();
   }

   public static long findBiggestDenominator(long a, long b) {
      if (b == 0) return a;
      return findBiggestDenominator(b, a % b);
   }

   public void simplify() {
      long newNum = numerator;
      long newDenom = denominator;

      long denom = findBiggestDenominator(Math.abs(newNum), newDenom);

      numerator = newNum / denom;
      denominator = newDenom / denom;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return numerator + "/" + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

      if (m instanceof Lfraction) {
         return compareTo((Lfraction) m) == 0;
      }
      return false;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {

       return Objects.hash(numerator, denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long newNum1;
      long newNum2;
      long newDenom;
      long result;

      if (denominator == m.getDenominator()) {  // 1/3 + 1/3
         newDenom = denominator;
      } else if (denominator % m.getDenominator() == 0 || m.getDenominator() % denominator == 0) {  // 1/3 + 1/6
         newDenom = Math.max(denominator, m.getDenominator());
      } else {
         newDenom = denominator * m.getDenominator();  // 1/3 + 1/4
      }

      newNum1 = numerator * newDenom / denominator;
      newNum2 = m.getNumerator() * newDenom / m.getDenominator();

      result = newNum1 + newNum2;

      return new Lfraction(result, newDenom);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long newNum;
      long newDenom;

      if (numerator == denominator) {
         newNum = numerator;
         newDenom = denominator;
         return new Lfraction(newNum, newDenom);
      } else if (m.getNumerator() == m.getDenominator()) {
         newNum = m.getNumerator();
         newDenom = m.getDenominator();
         return new Lfraction(newNum, newDenom);
      }

      newNum = numerator * m.getNumerator();
      newDenom = denominator * m.getDenominator();

      return new Lfraction(newNum, newDenom);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {

      if (numerator == 0) {
         throw new RuntimeException("numerator is 0. cannot inverse and divide by 0");
      }

      if (numerator < 0) {
         return new Lfraction(-denominator, -numerator);
      } else {
         return new Lfraction(denominator, numerator);
      }
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {

      return new Lfraction(-numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {

      return plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {

      if (m.getNumerator() == 0) {
         throw new RuntimeException("cannot divide by 0");
      }

      if (numerator == m.getNumerator() && denominator == m.getDenominator()) {
         return new Lfraction(1L, 1L);
      }

      return times(m.inverse());
   }

   public Lfraction pow(int n) {
      if (n == 0) {
         return new Lfraction(1, 1);
      }

      if (n == 1) {
         return new Lfraction(numerator, denominator);
      }

      if (n == -1) {
         return this.inverse();
      }

      if (n < 0) {
         return pow(Math.abs(n)).inverse();
      }

      return times(pow(n - 1));
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {

       long num1 = numerator * m.getDenominator();
       long num2 = m.getNumerator() * denominator;

      if (num1 < num2) {
         return -1;
      } else if (num1 > num2) {
         return 1;
      }
      return 0;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @SuppressWarnings("MethodDoesntCallSuperMethod")
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {

      if (Math.abs(numerator) > denominator) {
         return numerator / denominator;
      }
      return 0L;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {

      long result = Math.abs(numerator);

      while (result >= denominator) {
         result -= denominator;
      }
      if (numerator < 0) {
         return new Lfraction(-result, denominator);
      }

      return new Lfraction(result, denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {

      return (double) numerator / denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {

      long num;

      if (f < 0) {
         num = (long) (f * d - 0.5);
      } else {
      num = (long) (f * d + 0.5);
      }

      return new Lfraction(num, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

      try {
         String[] elements = s.split("/");

         if (elements.length > 2) {
            throw new RuntimeException("string " + s + " contains redundant / elements");
         }

         try {
            long num1 = Long.parseLong(elements[0]);
            long num2 = Long.parseLong(elements[1]);
            return new Lfraction(num1, num2);
         } catch (NumberFormatException e) {
            throw new RuntimeException("string " + s + "  contains illegal symbols");
         }
      } catch (ArrayIndexOutOfBoundsException e) {
         throw new RuntimeException("string " + s + " has no / element");
      }
   }
}

